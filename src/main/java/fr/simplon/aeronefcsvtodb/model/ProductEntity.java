package fr.simplon.aeronefcsvtodb.model;

import lombok.Builder;

import javax.persistence.*;

@Entity
@Builder
public class ProductEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private long ID;

    @Column
    private String active;

    @Column
    private String name;

    @Column
    private CategoryEntity[] categories;

    @Column
    private String priceTaxIncludedOrNot;

    @Column
    private String taxRules;

    @Column
    private String wholesalePrice;

    @Column
    private String onSale;

    @Column
    private String discountAmount;

    @Column
    private String discountPercentage;

    @Column
    private String discountFrom;

    @Column
    private String discountTo;

    @Column
    private String reference;

    @Column
    private String supplierReference;

    @Column
    private String supplier;

    @Column
    private String manufacturer;

    @Column
    private String EAN13;

    @Column
    private String UPC;

    @Column
    private String ecotax;

    @Column
    private String width;

    @Column
    private String height;

    @Column
    private String depth;

    @Column
    private String weight;

    @Column
    private String quantity;

    @Column
    private String minimalQuantity;

    @Column
    private String visibility;

    @Column
    private String shippingCost;

    @Column
    private String unity;

    @Column
    private String unitPriceRatio;

    @Column
    private String shortDescription;

    @Column
    private String description;

    @Column
    private String tags;

    @Column
    private String metaTitle;

    @Column
    private String metaKeywords;

    @Column
    private String metaDescription;

    @Column
    private String urlRewritten;

    @Column
    private String textWhenInStock;

    @Column
    private String textBackorderAllowed;

    @Column
    private String availableForOrder;

    @Column
    private String availableDate;

    @Column
    private String creationDate;

    @Column
    private String showPrice;

    @Column
    private String imagesUrl;

    @Column
    private String deleteExistingImages;

    @Column
    private String feature;

    @Column
    private String availableOnlineOnly;

    @Column
    private String conditionOfProduct;

    @Column
    private String customizable;

    @Column
    private String uploadableFiles;

    @Column
    private String textFields;

    @Column
    private String outOfStock;

    @Column
    private String nameOfShop;

    @Column
    private String advancedStockManagement;

    @Column
    private String dependsOn;

    @Column
    private String warehouse;

    @Column
    private String linkedProducts;

    @Column
    private String allowPreOrder;

    @Column
    private String allowPreOrderCountDown;

    @Column
    private String preorderDate;

    @Column
    private String preorderDateInitQuantity;
}
