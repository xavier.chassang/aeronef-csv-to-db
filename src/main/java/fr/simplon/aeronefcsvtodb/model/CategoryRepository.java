package fr.simplon.aeronefcsvtodb.model;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<CategoryEntity, Long> {
    public CategoryEntity findFirstByName(String name);
}
