package fr.simplon.aeronefcsvtodb.service;

import fr.simplon.aeronefcsvtodb.csvconvertor.CSVtoEntityMapper;
import fr.simplon.aeronefcsvtodb.csvconvertor.ProductCsv;
import fr.simplon.aeronefcsvtodb.model.ProductEntity;
import fr.simplon.aeronefcsvtodb.model.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;

    public ProductEntity saveProduct(ProductEntity pb) {
        return productRepository.save(pb);
    }

    public List<ProductEntity> saveProductToDbIfNotPresent(ProductCsv productCsv) {

        List<ProductEntity> savedProducts = new ArrayList<>();

        ProductEntity categoryEntitiesFromCsv = CSVtoEntityMapper.productCsvToProductEntities(productCsv);


        try {
            saveProduct(categoryEntitiesFromCsv);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return savedProducts;
    }

    /*//public void processListOfProductCsv(List<ProductCsv> productCsvList) {
        for (ProductCsv productCsv : productCsvList) {

            System.out.println("\n");
            System.out.println(productCsv.getName());
            System.out.println(productCsv.getReference());
            System.out.println(productCsv.getImagesUrl());
            try {

                ProductCsv result = this.saveProduct(productCsv);
                System.out.println();
                System.out.println("§§§§§§§§§§ SUCCES §§§§§§§§§§§ = " + result.getName());
                System.out.println();
            } catch (Exception e) {
                System.out.println();
                System.out.println("!!!!!!!!!!!!!!!! ERROR FOR = " + productCsv + " : " + e.getMessage() + " !!!!!!!!!!!!!!!!!!");
                System.out.println();
            }

        }
    }*/
}
