package fr.simplon.aeronefcsvtodb.service;

import fr.simplon.aeronefcsvtodb.csvconvertor.CSVtoEntityMapper;
import fr.simplon.aeronefcsvtodb.csvconvertor.ProductCsv;
import fr.simplon.aeronefcsvtodb.model.CategoryEntity;
import fr.simplon.aeronefcsvtodb.model.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryService {

    private final CategoryRepository categoryRepository;

    public List<CategoryEntity> saveCategoryToDbIfNotPresent(ProductCsv productCsv) {

        List<CategoryEntity> savedCategories = new ArrayList<>();

        List<CategoryEntity> categoryEntitiesFromCsv = CSVtoEntityMapper.productCsvToCategoryEntities(productCsv);

        for(CategoryEntity categoryEntity : categoryEntitiesFromCsv) {
            try {
                if(categoryRepository.findFirstByName(categoryEntity.getName()) == null) {
                    CategoryEntity savedCategoryEntity = categoryRepository.save(categoryEntity);
                    savedCategories.add(savedCategoryEntity);
                }

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        return savedCategories;
    }
}
