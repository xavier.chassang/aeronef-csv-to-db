package fr.simplon.aeronefcsvtodb;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AeronefBOApp implements CommandLineRunner {
	
	public static void main(String[] args) throws Exception {
		SpringApplication.run(AeronefBOApp.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

	}
}
