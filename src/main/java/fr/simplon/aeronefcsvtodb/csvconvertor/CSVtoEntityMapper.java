package fr.simplon.aeronefcsvtodb.csvconvertor;

import fr.simplon.aeronefcsvtodb.model.CategoryEntity;
import fr.simplon.aeronefcsvtodb.model.ProductEntity;

import java.util.Arrays;
import java.util.List;

public class CSVtoEntityMapper {

    public static ProductEntity productCsvToProductEntities(ProductCsv productCsv) {
        return ProductEntity.builder()
                .name(productCsv.getName())
                .build();
    }

    public static List<CategoryEntity> productCsvToCategoryEntities(ProductCsv productCsv) {
        return Arrays.stream(productCsv.getCategories().split(","))
                .map(string ->
                        CategoryEntity.builder()
                                .name(string)
                                .build())
                .toList();
    }
}
