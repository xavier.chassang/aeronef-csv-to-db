package fr.simplon.aeronefcsvtodb.csvconvertor;

import com.opencsv.bean.CsvBindByName;
import lombok.Data;

@Data
public class ProductCsv {

		@CsvBindByName(column = "ID")
		private String ID;
		
		@CsvBindByName(column = "Active (0/1)")
		private String active;
	
		@CsvBindByName(column = "Name *")
		private String name;
		
		@CsvBindByName(column = "Categories (x,y,z...)")
		private String categories;
		
		@CsvBindByName(column = "Price tax excluded or Price tax included")
		private String priceTaxIncludedOrNot;
		
		@CsvBindByName(column = "Tax rules ID")
		private String taxRules;
		
		@CsvBindByName(column = "Wholesale price")
		private String wholesalePrice;
		
		@CsvBindByName(column = "On sale (0/1)")
		private String onSale;
		
		@CsvBindByName(column = "Discount amount")
		private String discountAmount;
		
		@CsvBindByName(column = "Discount percent")
		private String discountPercentage;
		
		@CsvBindByName(column = "Discount from (yyyy-mm-dd)")
		private String discountFrom;
		
		@CsvBindByName(column = "Discount to (yyyy-mm-dd)")
		private String discountTo;
	
		@CsvBindByName(column = "Reference #")
		private String reference;
		
		@CsvBindByName(column = "Supplier reference #")
		private String supplierReference;
		
		@CsvBindByName(column = "Supplier")
		private String supplier;
		
		@CsvBindByName(column = "Manufacturer")
		private String manufacturer;
		
		@CsvBindByName(column = "EAN13")
		private String EAN13;
		
		@CsvBindByName(column = "UPC")
		private String UPC;
		
		@CsvBindByName(column = "Ecotax")
		private String ecotax;
		
		@CsvBindByName(column = "Width")
		private String width;
		
		@CsvBindByName(column = "Height")
		private String height;
		
		@CsvBindByName(column = "Depth")
		private String depth;
		
		@CsvBindByName(column = "Weight")
		private String weight;
		
		@CsvBindByName(column = "Quantity")
		private String quantity;
		
		@CsvBindByName(column = "Minimal quantity")
		private String minimalQuantity;
		
		@CsvBindByName(column = "Visibility")
		private String visibility;
		
		@CsvBindByName(column = "Additional shipping cost")
		private String shippingCost;
		
		@CsvBindByName(column = "Unity")
		private String unity;
		
		@CsvBindByName(column = "Unit price ratio")
		private String unitPriceRatio;
		
		@CsvBindByName(column = "Short description")
		private String shortDescription;
		
		@CsvBindByName(column = "Description")
		private String description;
		
/*		@CsvBindByName(column = "Tags (x,y,z...)")
		private String tags;*/
		
		@CsvBindByName(column = "Meta title")
		private String metaTitle;
		
		@CsvBindByName(column = "Meta,keywords,attention,enlever,trop")
		private String metaKeywords;
		
		@CsvBindByName(column = "Meta description")
		private String metaDescription;
		
		@CsvBindByName(column = "URL-rewritten")
		private String urlRewritten;
		
		@CsvBindByName(column = "Text when in stock")
		private String textWhenInStock;
		
		@CsvBindByName(column = "Text when backorder allowed")
		private String textBackorderAllowed;
		
		@CsvBindByName(column = "Available for order (0 = No, 1 = Yes)")
		private String availableForOrder;
		
		@CsvBindByName(column = "Product available date")
		private String availableDate;
		
		@CsvBindByName(column = "Product creation date")
		private String creationDate;
		
		@CsvBindByName(column = "Show price (0 = No, 1 = Yes)")
		private String showPrice;
		
		@CsvBindByName(column = "Image URLs (x.jpg.jpg.jpg.jpg, ../img_product/ ../img_product/ ../img_product/ ../img_product/y.jpg.jpg.jpg.jpg, ../img_product/ ../img_product/ ../img_product/ ../img_product/z...)")
		private String imagesUrl;
		
		@CsvBindByName(column = "Delete existing images (0 = No, 1 = Yes)")
		private String deleteExistingImages;
		
		@CsvBindByName(column = "Feature(Name:Value:Position)")
		private String feature;
		
		@CsvBindByName(column = "Available online only (0 = No, 1 = Yes)")
		private String availableOnlineOnly;
		
		@CsvBindByName(column = "Condition")
		private String conditionOfProduct;
		
		@CsvBindByName(column = "Customizable (0 = No, 1 = Yes)")
		private String customizable;
		
		@CsvBindByName(column = "Uploadable files (0 = No, 1 = Yes)")
		private String uploadableFiles;
		
		@CsvBindByName(column = "Text fields (0 = No, 1 = Yes)")
		private String textFields;
		
		@CsvBindByName(column = "Out of stock")
		private String outOfStock;
		
		@CsvBindByName(column = "ID / Name of shop")
		private String nameOfShop;
		
		@CsvBindByName(column = "Advanced stock management")
		private String advancedStockManagement;
		
		@CsvBindByName(column = "Depends On Stock")
		private String dependsOn;
		
		@CsvBindByName(column = "Warehouse")
		private String warehouse;
		
		@CsvBindByName(column = "linked products")
		private String linkedProducts;
		
		@CsvBindByName(column = "allow pre-order (0/1)")
		private String allowPreOrder;
		
		@CsvBindByName(column = "allow preorder countdown (0/1)")
		private String allowPreOrderCountDown;
		
		@CsvBindByName(column = "preorder date available")
		private String preorderDate;
		
		@CsvBindByName(column = "preorder Init Qty")
		private String preorderDateInitQuantity;
		
}
