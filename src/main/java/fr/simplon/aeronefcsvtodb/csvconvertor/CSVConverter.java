package fr.simplon.aeronefcsvtodb.csvconvertor;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class CSVConverter {

	public List<ProductCsv> serializeCsvToProductCsv(String path) {

		try {
			CsvToBean<ProductCsv> cb = new CsvToBeanBuilder<ProductCsv>(new FileReader(path))
					.withSeparator(';')
					.withType(ProductCsv.class)
					.build();

			return cb.parse();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return new ArrayList<>();
	}

}
