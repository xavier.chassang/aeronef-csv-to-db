package fr.simplon.aeronefcsvtodb.application;

import fr.simplon.aeronefcsvtodb.csvconvertor.CSVConverter;
import fr.simplon.aeronefcsvtodb.csvconvertor.ProductCsv;
import fr.simplon.aeronefcsvtodb.model.CategoryEntity;
import fr.simplon.aeronefcsvtodb.model.ProductEntity;
import fr.simplon.aeronefcsvtodb.service.CategoryService;
import fr.simplon.aeronefcsvtodb.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
@RequiredArgsConstructor
public class ServiceManager {

    private final ProductService productService;

    private final CategoryService categoryService;

    private String pathTest = "C:\\Users\\Pantatruelle\\eclipse-workspace\\aeronef-csv-to-db\\src\\main\\java\\fr\\simplon\\aeronefcsvtodb\\2015-04-28-chrgt-02EN-AirModUSa.csv";

    @PostConstruct
    public void run() {

        List<ProductCsv> productCsvList = new CSVConverter().serializeCsvToProductCsv(pathTest);

        for (ProductCsv productCsv : productCsvList) {

            try {

                //ProductCsv result = productService.saveProduct(productCsv);
                List<ProductEntity> productEntityList = productService.saveProductToDbIfNotPresent(productCsv);
                List<CategoryEntity> categoryEntityList = categoryService.saveCategoryToDbIfNotPresent(productCsv);

                for (CategoryEntity category:
                        categoryEntityList) {
                    System.out.println();
                    System.out.println(category.getName());
                    System.out.println();
                }
				/*System.out.println();
				System.out.println("§§§§§§§§§§ SUCCES §§§§§§§§§§§ = " + result.getName());
				System.out.println();*/
            } catch (Exception e) {
                System.out.println();
                System.out.println("!!!!!!!!!!!!!!!! ERROR FOR = " + productCsv + " : " + e.getMessage() + " !!!!!!!!!!!!!!!!!!");
                System.out.println();
            }

        }
    }

}
